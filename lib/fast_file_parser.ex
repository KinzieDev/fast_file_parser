defmodule FastFileParser do
  @moduledoc """
  Documentation for `FastFileParser`.
  """

  def get_info({:image, %{content_type: "image/jpeg"}, extra_data}, binary) do
    prev_binary = extra_data[:prev_binary]
    binary = if prev_binary, do: prev_binary <> binary, else: binary

    get_jpg_data(binary, extra_data[:b_len])
  end

  def get_info(_, binary) do
    get_content_type(binary)
  end

  defp get_jpg_data(<<binary::binary>>, b_len) do
    binary_byte_size = byte_size(binary)

    if b_len + 9 >= binary_byte_size do
      if b_len > binary_byte_size do
        {:cont, {:image, %{content_type: "image/jpeg"}, %{b_len: b_len - binary_byte_size}}}
      else
        {:cont, {:image, %{content_type: "image/jpeg"}, %{b_len: b_len, prev_binary: binary}}}
      end
    else
      case binary do
        <<_::bytes-size(b_len), 0xFF, sof::size(8), rest::binary>> ->
          if sof in [0xC2, 0xC0] do
            case rest do
              <<_skip::size(24), height::size(16), width::size(16), _::binary>> ->
                {:halt, {:image, %{content_type: "image/jpeg", width: width, height: height}}}

              _ ->
                {:halt, {:error, :file_corrupted}}
            end
          else
            <<b_len::size(16), next::binary>> = rest
            get_jpg_data(next, b_len - 2)
          end

        _ ->
          {:halt, {:error, :file_corrupted}}
      end
    end
  end

  def get_content_type(binary) do
    case binary do
      <<0xFFD8::size(16), _block::size(16), rest::binary>> ->
        <<b_len::size(16), rest::binary>> = rest
        get_jpg_data(rest, b_len - 2)

      <<0x89, "PNG", 0x0D, 0x0A, 0x1A, 0x0A, rest::binary>> ->
        <<_data::size(32), "IHDR", width::size(32), height::size(32), _rest::binary>> = rest
        {:halt, {:image, %{content_type: "image/png", width: width, height: height}}}

      <<"GIF87a", rest::binary>> ->
        <<width::little-size(16), height::little-size(16), _rest::binary>> = rest
        {:halt, {:image, %{content_type: "image/gif", width: width, height: height}}}

      <<"GIF89a", rest::binary>> ->
        <<width::little-size(16), height::little-size(16), _rest::binary>> = rest
        {:halt, {:image, %{content_type: "image/gif", width: width, height: height}}}

      <<"%PDF", _data::binary>> ->
        {:halt, {:document, %{content_type: "application/pdf"}}}

      _ ->
        {:halt, {:error, :unsupported}}
    end
  end

  @doc """
  Gets the checksum of a file by path and returns it in binary form
  """
  @spec get_checksum(String.t(), atom()) :: binary()
  def get_checksum(path, hash \\ :sha256) do
    File.stream!(path, [], 2048)
    |> Enum.reduce(:crypto.hash_init(hash), &:crypto.hash_update(&2, &1))
    |> :crypto.hash_final()
  end
end
