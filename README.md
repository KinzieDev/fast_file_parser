# FastFileParser

Fast file parser written in Elixir.

## Installation

The package hasn't been published to hex.pm as of yet, but you can fetch it directly from the git repository by including the following text in your mix.exs file:

```elixir
def deps do
  [
    {:fast_file_parser, git: "https://codeberg.org/Kinzie/fast_file_parser.git", tag: "0.1.0"}
  ]
end
```

## Usage

```elixir
def get_file_info(path) do
  File.stream!(path, [], 2048)
  |> Enum.reduce_while({nil, %{}, nil}, &FastFileParser.get_info(&2, &1))
end
```

### Examples

```elixir 
get_file_info("image.jpg")
>> {:image, %{content_type: "image/jpeg", width: 512, height: 512}}

get_file_info("doc.pdf")
>> {:document, %{content_type: "application/pdf"}}

get_file_info("img.svg")
>> {:error, :unsupported}
```
